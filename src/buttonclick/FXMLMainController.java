/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buttonclick;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;


/**
 *
 * @author olandol
 */
public class FXMLMainController implements Initializable {
    
 @FXML public  AnchorPane ap;
 //@FXML public GridPane gp; 
 //@FXML public Label lbl;
 Random random = new Random(System.currentTimeMillis());

    

 
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
        ap.requestFocus();

        ap.setStyle("-fx-background-color: rgb(" + String.valueOf(random.nextInt(255)) + ","
                + String.valueOf(random.nextInt(255)) + ","
                + String.valueOf(random.nextInt(255)) + ");");
        ap.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                ap.setStyle("-fx-background-color: rgb(" + String.valueOf(random.nextInt(255)) + ","
                        + String.valueOf(random.nextInt(255)) + ","
                        + String.valueOf(random.nextInt(255)) + ");");
                playsignal("resource/sound" + String.valueOf(random.nextInt(4)+1) +  ".mp3"); 
                /*try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    
                }*/
            }
        });

        ap.setOnMouseClicked(new EventHandler<MouseEvent>() { 
            @Override
            public void handle(MouseEvent event) {
                ap.setStyle("-fx-background-color: rgb(" + String.valueOf(random.nextInt(255)) + ","
                        + String.valueOf(random.nextInt(255)) + ","
                        + String.valueOf(random.nextInt(255)) + ");");
                playsignal("resource/sound" + String.valueOf(random.nextInt(4)+1) +  ".mp3"); 
                
                Scene scene = (Scene) ap.getScene();
                Image image_cursor = new Image(getClass().getResource("resource/" 
                        + String.valueOf(random.nextInt(14)+1) +".png").toExternalForm());
                scene.setCursor(new ImageCursor(image_cursor));
            }
        });
        
    }
  
    private void playsignal (String path){
        final URL resource = getClass().getResource(path);
        final Media media = new Media(resource.toString());
        final MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.play();
      }
    
    
    
}
