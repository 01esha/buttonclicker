/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buttonclick;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.ImageCursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author olandol
 */
public class ButtonClick extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLMain.fxml"));
        stage.initStyle(StageStyle.DECORATED);
        stage.setFullScreen(true);
        Image image_cursor = new Image(getClass().getResource("resource/2.png").toExternalForm());          
        Scene scene = new Scene(root);
        scene.setCursor(new ImageCursor(image_cursor));
        scene.getRoot().requestFocus();
        stage.setTitle("Кнопконажималка");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("resource/icon.png")));
        stage.setScene(scene);
        stage.show();                
        
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
